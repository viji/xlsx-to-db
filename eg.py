import xlrd
import MySQLdb
xl = xlrd.open_workbook("eg.xls")
sheet = xl.sheet_by_name("eg")
database = MySQLdb.connect (host="localhost", user = "root", passwd = "root", db = "test")
cursor = database.cursor()
query = """INSERT INTO orders (name,rollno) VALUES (%s, %s)"""
for r in range(1, sheet.nrows):
    name=sheet.cell(r,0).value
    rollno=sheet.cell(r,1).value
    values = (name,rollno)
    cursor.execute(query, values)
cursor.close()
database.commit()
database.close()
columns = str(sheet.ncols)
rows = str(sheet.nrows)
